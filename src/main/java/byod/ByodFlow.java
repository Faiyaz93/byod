package byod;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class ByodFlow {

	public static void main(String[] args) {
		// TODO Auto-generated method stub		
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\faiyazs\\Desktop\\Selenium_Everything\\work-space\\byod\\chrome\\chromedriver.exe");
		WebDriver driver= new ChromeDriver();
		
		driver.get("https://tst12.stage.att.com/");
		System.out.println("browser launched");
		WebElement shopSupport =driver.findElement(By.className("icon-hamburger"));
		shopSupport.click();
		System.out.println("Shop & Support clicked");
		WebElement wireless =driver.findElement(By.xpath("//*[@id=\"tab-desktop-menu\"]/li[14]/a"));
		wireless.click();
		System.out.println("Wireless is clicked");
		driver.manage().window().maximize();
		
		WebElement byod =driver.findElement(By.xpath("//*[@id='tab-desktop-menu']/li[14]/ul/li[9]/a[1]"));
		byod.click();
		System.out.println("Bring your own device is clicked");
		driver.get("https://tst12.stage.att.com/buy/byod");
		
		driver.manage().window().maximize() ;
		
		WebElement getStarted =driver.findElement(By.xpath("//*[@id=\"__next\"]/div[2]/div/div[1]/div/div[5]/button"));
		getStarted.click();
		System.out.println("GetStarted is clicked");
		
		driver.manage().window().maximize() ;
		WebElement phones =driver.findElement(By.xpath("//*[@id='__next']/div[2]/div/div[2]/div[1]/div[1]/div[3]/ul/li[1]/div/div"));
		phones.click();
		driver.manage().window().maximize() ;
		WebElement phones_continue =driver.findElement(By.xpath("//*[@class='_2aMhj _1tc-v false _1cMDJ _2kufz _sqgDl']"));
		phones_continue.click();
		
		System.out.println("Land to Sim-selection page");		
		
		WebElement ordNwSIM =driver.findElement(By.xpath("//*[text()='Order a new SIM card.']"));
		ordNwSIM.click();
		
		WebElement simSelection_continue =driver.findElement(By.xpath("//*[@id=\"__next\"]/div[2]/div/div[2]/div[1]/div[2]/div[3]/div/div[2]/button"));
		simSelection_continue.click();
		
		WebElement modelContinue =driver.findElement(By.xpath("//div[contains(@data-qa,'modalContent')]//button[contains(text(),'Continue')]"));
		modelContinue.click();
		
		System.out.println("New flow ");		
		
	}

}
