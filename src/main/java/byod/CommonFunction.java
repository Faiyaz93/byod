package byod;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CommonFunction {
	WebDriver driver;

	public CommonFunction(WebDriver driver) {
		this.driver = driver;
	}
	
	public WebElement getWebElement(String path) {
		return driver.findElement(By.xpath(path));
				
	}
	public void update(WebElement el,String value) {
		el.sendKeys(value);		
	}
	
//	public void launchDriver() {
//		System.setProperty("webdriver.chrome.driver", "C:\\Users\\faiyazs\\Desktop\\Selenium_Everything\\work-space\\byod\\chrome\\chromedriver.exe");
//		this.driver= new ChromeDriver();
//	}
	
	public boolean openPage(String url, String pageTitle) {
		driver.manage().deleteAllCookies();
		driver.get(url);
		if(driver.getTitle().equalsIgnoreCase(pageTitle)) {
			return true;
		}
		return false;
	}
	
	public void sleep() {
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
	}
	
	public void waitforElement(String xPath,WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver,15);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xPath)));		
	}
	
}
