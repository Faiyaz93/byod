package byod;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CustomerInformation {
	String firstNameXpath="//input[contains(@name,'firstName')]";
	String lastNameXpath="//input[contains(@name,'lastName')]";
	String phoneNumberXpath= "//input[contains(@name,'primaryContact')]";
	String radio_NoXpath ="//input[contains(@id,'unauthctnOptInNo')]";
	String emailXpath ="//input[contains(@name,'email')]";
	public String contactInfo_nextXpath="//span[contains(text(),'Next')]";
	String dobXpath ="//input[contains(@name,'dob')]";
	String ssnXpath="//input[contains(@name,'ssn')]";
	public String credit_NextXpath="//div[contains(@class,'cta-button-group nodots mTop20')]/button/span";
	String line1Xpath="//input[contains(@aria-describedby,'addressLine1')]";
	String cityXpath="//input[contains(@id,'city1')]";
	String stateButtonXpath="//button[contains(@aria-label,' Select a state')]";
	String statedrpdownXpath="//li[text()='Texas']";
	String zipXpath="//input[contains(@id,'zipCode')]";
	public String addressButtonXpath="//span[contains(text(),'Verify address')]";
	public String checkoutContinueXpath="//button[text()='Continue']";////ccc-module-control/div
	String newNumberXpath="//*[contains(@id,'displayAssignNewNumberRadio')]";
	String ContinueCTAxPath="//*[contains(text(),'Continue')]";
	String cardNumberXpath="//*[contains(@name,'cardNumber')]";
	String monthXpath="//button[text()='Month']";
	String monthXpathSelect="//li[text()='07 (Jul)']";
	String yearXpath ="//button[text()='Year']";
	String yearSelectXpath="//li[text()='2019']";
	String securityCodeXpath="//*[contains(@placeholder,'3 digits')]";
	String nextCTAXpath="//span[contains(text(),'Next')]";
	//String finalCheckoutCTAXpath="//span[contains(text(),'Continue')]";
	String submitOrderCTAXpath="//button[contains(text(),'Submit order')]";
	String paymentContinueXpath="/html/body/div[2]/div/div/payment-terms/div[4]/common-component-ctas/div[2]/button[1]/span";
	
	WebDriver driver;
	CommonFunction objCommonFunction;
	
	public CustomerInformation(WebDriver driver){
		this.driver=driver;
		objCommonFunction= new CommonFunction(driver);
	}
	public boolean contactInfo() {
		WebElement firstName=objCommonFunction.getWebElement(firstNameXpath);
		WebElement lastName=objCommonFunction.getWebElement(lastNameXpath);
		WebElement phoneNumber=objCommonFunction.getWebElement(phoneNumberXpath);
		WebElement radio_No=objCommonFunction.getWebElement(radio_NoXpath);
		WebElement email=objCommonFunction.getWebElement(emailXpath);
		WebElement contactInfo_next=objCommonFunction.getWebElement(contactInfo_nextXpath);
		if(firstName==null || lastName==null || phoneNumber==null|| radio_No==null|| email==null || contactInfo_next==null )
			return false;
		else {
			objCommonFunction.update(firstName, "Faiyaz");
			objCommonFunction.update(lastName, "Sayeed");
			objCommonFunction.update(phoneNumber, "5678430942");
			radio_No.click();
			objCommonFunction.update(email, "fs5163@att.com");
			contactInfo_next.click();
			return true;			
	    }
	}
	
	public boolean creditCheck() {
		WebElement dob=objCommonFunction.getWebElement(dobXpath);
		WebElement ssn=objCommonFunction.getWebElement(ssnXpath);
		WebElement credit_Next=objCommonFunction.getWebElement(credit_NextXpath);
		if(dob==null || ssn==null )
			return false;
		else {
		objCommonFunction.update(dob, "11/28/1950");
		objCommonFunction.update(ssn, "326000094");
		credit_Next.click();		
		return true;
		}
	}
	public boolean shippingAddress() {
		WebDriverWait wait = new WebDriverWait(driver,10);
		
		WebElement line1=objCommonFunction.getWebElement(line1Xpath);
		WebElement city=objCommonFunction.getWebElement(cityXpath);
		WebElement stateButton=objCommonFunction.getWebElement(stateButtonXpath);
		WebElement statedrpdown=objCommonFunction.getWebElement(statedrpdownXpath);
		WebElement zip=objCommonFunction.getWebElement(zipXpath);
		WebElement addressButton=objCommonFunction.getWebElement(addressButtonXpath);
		if(line1==null || city==null|| stateButton==null ||zip==null|| addressButton==null)
			return false;
		else {
			objCommonFunction.update(line1, "9337 Chimney sweep ln");
			objCommonFunction.update(city, "DALLAS");
			stateButton.click();
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(statedrpdownXpath)));
			statedrpdown.click();
			objCommonFunction.update(zip, "75243");
			addressButton.click();
			return true;
		}		
	}
	public boolean checkoutSubmit() {
		WebElement checkoutContinue=objCommonFunction.getWebElement(checkoutContinueXpath);
        if(checkoutContinue==null)
        	return false;
        else {
        	checkoutContinue.click();
        	return true;
        }
		
	}
	public boolean selectANumber() {
		WebElement newNumber=objCommonFunction.getWebElement(newNumberXpath);
		WebElement ContinueCTA=objCommonFunction.getWebElement(ContinueCTAxPath);
		if(newNumberXpath==null || ContinueCTA==null)
			return false;
		else {
			newNumber.click();
			ContinueCTA.click();
			return true;
		}
	}
	public boolean submitPaymentDetails() {
		WebElement cardNumber=objCommonFunction.getWebElement(cardNumberXpath);
		WebElement month=objCommonFunction.getWebElement(monthXpath);
		WebElement monthSelect=objCommonFunction.getWebElement(monthXpathSelect);
		WebElement year=objCommonFunction.getWebElement(yearXpath);
		WebElement yearSelect=objCommonFunction.getWebElement(yearSelectXpath);
		WebElement securityCode=objCommonFunction.getWebElement(securityCodeXpath);
		WebElement nextCTA=objCommonFunction.getWebElement(nextCTAXpath);
				
		if(nextCTA==null||cardNumber==null||month==null||securityCode==null)
			return false;
		else {
			objCommonFunction.update(cardNumber, "5507033327932107");
			
			month.click();
			monthSelect.click();
			year.click();
			yearSelect.click();
			objCommonFunction.update(securityCode, "123");
			nextCTA.click();
			return true;			
		}		
	}
	public boolean submitCheckout() {
		WebElement paymentContinueCTA= objCommonFunction.getWebElement(paymentContinueXpath);
		if(paymentContinueCTA==null)
			return false;
		else {
			paymentContinueCTA.click();
			return true;
		}
	}
	public boolean submitOrder() {
		WebElement submitOrderCTA= objCommonFunction.getWebElement(submitOrderCTAXpath);
		if(submitOrderCTA==null)
			return false;
		else {
			submitOrderCTA.click();
			return true;
		}
	}
}
