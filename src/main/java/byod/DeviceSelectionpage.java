package byod;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class DeviceSelectionpage {
	WebDriver driver;
	CommonFunction objCommonFunction;
	
	public DeviceSelectionpage(WebDriver driver){
		this.driver=driver;
        objCommonFunction= new CommonFunction(this.driver);	
		
	}
	
	//String getStarted="//button[contains(@class,'_UKDoQ _1tmtr false  _1u716 _1C3w8')]/span";
	String getStarted="//span[text()='Get started']";  //prod
	//String phones ="//ul[@data-qa='byod-select-device']/li[1]";test
	public String phones="//div[text()='Phones']";
	//String phones="//ul[@data-qa='byod-select-device']//div[text()='Phones']";
	public String ds_continue= "//div[@data-qa='byod-navigation']/button";
	
	
	public boolean clickOnGetStarted() {
		
		WebElement getStartedCTA = objCommonFunction.getWebElement(getStarted);
		driver.manage().window().maximize();
		if(getStartedCTA == null)
			return false;
		else {
			getStartedCTA.click();
			return true;
		}
		
	}
	
	public boolean clickOnPhones() {
		objCommonFunction.waitforElement(phones, driver);		
		WebElement phonesCTA =driver.findElement(By.xpath(phones));
		if(phonesCTA==null) {
			return false;			
		}
		else {
			//phonesCTA.sendKeys(Keys.RETURN);
			//new Actions(driver).moveToElement(phonesCTA).click().build().perform();
			//phonesCTA.click();
			
			//JavascriptExecutor executor = (JavascriptExecutor)driver;
			//executor.executeScript("arguments[0].click();", phonesCTA);
			phonesCTA.click();
			return true;
		}
	}
	
	public boolean clickOnContinue() {
		//objCommonFunction.waitforElement(ds_continue, driver);
		objCommonFunction.waitforElement(ds_continue, driver);
		WebElement continueCTA = driver.findElement(By.xpath(ds_continue));
		if(continueCTA== null) {
			return false;
		}
		else {
			try {
				/*JavascriptExecutor executor = (JavascriptExecutor)driver;
				executor.executeScript("arguments[0].click();", continueCTA);*/
				continueCTA.click();
				//new Actions(driver).moveToElement(continueCTA).click().build().perform();
			}
			catch(Exception e) {
				System.out.println(e);
				return false;
			}						
			return true;
		}
		
	}
	
}
