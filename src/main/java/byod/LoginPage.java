package byod;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPage {
	String userId="//input[@id='userid']";
	String password="//input[@id='password']";
	String logIn="//input[@id='tguardLoginButton']";
	String userIdText="//div[text()='User ID']"; 
	WebDriver driver;
	CommonFunction objCommonFunction;
	public LoginPage(WebDriver driver) {
		this.driver=driver;
		objCommonFunction= new CommonFunction(driver);
	}
	
	public boolean login() {
		WebElement userIdTextCTA= objCommonFunction.getWebElement(userIdText);
		WebElement userIdCTA= objCommonFunction.getWebElement(userId);
		WebElement passwordCTA= objCommonFunction.getWebElement(password);
		WebElement logInCTA= objCommonFunction.getWebElement(logIn);
		if(userIdCTA==null || passwordCTA==null || logInCTA==null) {
		return false;
		}
		else {
			userIdTextCTA.click();
			objCommonFunction.update(userIdCTA, "6822158359");
			objCommonFunction.update(passwordCTA, "tester");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			logInCTA.click();
			return true;
		}
		
	}
	
	

}
