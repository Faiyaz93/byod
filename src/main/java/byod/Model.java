package byod;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Model {
	WebDriver driver;
	CommonFunction objCommonFunction;
	
	String md_continue = "//button[contains(@data-qa,'ByodPage-intentModal--intentButton-Continue')]";
	String zipCode="//input[contains(@id,'zipCodeEntry')]";
	public String zipCodeCTA="//input[contains(@id,'zipSubmit')]";
	String signIn="//button[contains(text(),'Sign in')]";
	
	
	public Model(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver=driver;
		objCommonFunction= new CommonFunction(driver);
	}
	

	public boolean clickOnMdcontinue() {
		
		WebElement ContinueCTA;
		WebDriverWait wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(md_continue)));
		ContinueCTA=objCommonFunction.getWebElement(md_continue);
		
		if(ContinueCTA==null)
			return false;
		else {
			/*JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", ContinueCTA);*/
			ContinueCTA.click();
			//new Actions(driver).moveToElement(ContinueCTA).click().build().perform();
			return true;
		}
		
	}
	public boolean clickOnSignIn() {
		WebElement signInCTA;
		WebDriverWait wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(signIn)));
		signInCTA=objCommonFunction.getWebElement(signIn);	
		if(signInCTA==null) {
			return false;
		}
		else {
			//signInCTA.sendKeys(Keys.ENTER);
			signInCTA.click();
			return true;
		}
	}
	public boolean enterZipCode() {
		WebDriverWait wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(zipCode)));
		WebElement input=objCommonFunction.getWebElement(zipCode);
		WebElement submitCTA=objCommonFunction.getWebElement(zipCodeCTA);
		
		if(input==null) {
			return false;
		}
		else {
			input.sendKeys("75081");
			submitCTA.click();			
			return true;
		}
		
	}

}
