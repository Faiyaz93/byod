package byod;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MyCart {
	public String checkoutXpath ="//div[contains(@class,' align-links-cartTop')]/button";
	WebDriver driver;
	CommonFunction objCommonFunction;
	public MyCart(WebDriver driver) {
		this.driver=driver;
		objCommonFunction= new CommonFunction(driver);
	}
	
	public boolean clickOnCheckout() {
		WebElement checkoutCTA=objCommonFunction.getWebElement(checkoutXpath);
		WebDriverWait wait = new WebDriverWait(driver,20);
		wait.until(ExpectedConditions.visibilityOf(checkoutCTA));
		//WebElement checkoutCTA=objCommonFunction.getWebElement(checkoutXpath);
		if(checkoutCTA==null) {
			return false;
		}
		else {
			new Actions(driver).moveToElement(checkoutCTA).click().build().perform();
			//checkoutCTA.click();
			return true;
		}
	}
}
