package byod;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class NumberSelectionPage {
	WebDriver driver;
	CommonFunction objCommonFunction;
	String newNumber="//input[@id='displayAssignNewNumberRadio']";
	String checkout="//button[text()='Checkout']";
	
	
	public NumberSelectionPage(WebDriver driver){
		this.driver=driver;
		objCommonFunction= new CommonFunction(driver);	
	}
	
	public boolean selectNewNo() {
		WebElement newNumberRadio =objCommonFunction.getWebElement(newNumber);
		WebElement checkoutCTA =objCommonFunction.getWebElement(checkout);
		if(newNumberRadio ==null || checkoutCTA==null) {
			return false;
		}
		else {
			newNumberRadio.click();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			checkoutCTA.click();
			return true;
		}
	}
}
