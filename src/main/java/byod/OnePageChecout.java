package byod;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class OnePageChecout {
	String payment="//a[text()='Enter payment info']";
	String cardNo="//input[@name='cardNumber']";
	String monthXpath="//*[contains(@aria-label,'Expiration Month Month')]";
	String yearXpath ="//Button[contains(@aria-label,'Expiration Year Year')]";
	String securityCodeXpath="//*[contains(@placeholder,'3 digits')]";
	WebDriver driver;
	CommonFunction objCommonFunction;
	
	public OnePageChecout(WebDriver driver){
		this.driver=driver;
		objCommonFunction= new CommonFunction(driver);	
	}
	public boolean submitPaymentDetails() {
		WebElement paymentLink= objCommonFunction.getWebElement(payment);
		WebElement cardNoInput= objCommonFunction.getWebElement(cardNo);
		WebElement month=objCommonFunction.getWebElement(monthXpath);
		WebElement year=objCommonFunction.getWebElement(yearXpath);
		WebElement securityCode=objCommonFunction.getWebElement(securityCodeXpath);
		Select objMonth = new Select(month);
		Select objYear = new Select(year);
		
		if(cardNoInput==null||month==null||securityCode==null)
			return false;
		else {
			paymentLink.click();
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			objCommonFunction.update(cardNoInput, "5442981111111114");
			objMonth.selectByValue("12 (Dec)");
			objYear.selectByValue("2019");
			objCommonFunction.update(securityCode, "234");
			return true;			
		}		
	}

}
