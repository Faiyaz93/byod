package byod;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PlanPage {
	WebDriver driver;
	//String planCTA= "//*[contains(text(),'Mobile Share Plus(SM) 9GB')]//../..//*[contains(text(),'Select this plan')]";
	String planCTA="//button[text()='Select this plan']";
	
	CommonFunction objCommonFunction;
	
	public PlanPage(WebDriver driver) {
		this.driver= driver;
		objCommonFunction=new CommonFunction(this.driver);
	}
	public boolean clickOnPlanCTA() {
		WebDriverWait wait = new WebDriverWait(driver,20);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(planCTA)));
		WebElement selectPlanCTA=objCommonFunction.getWebElement(planCTA);
		if(selectPlanCTA==null) {
			return false;
		}
		else {
			//new Actions(driver).moveToElement(selectPlanCTA).click().build().perform();
			selectPlanCTA.click();
			System.out.println("Plan is selected");
			return true;			
		}
		
	}
	
	

}
