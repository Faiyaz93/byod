package byod;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class SimSelection {
	WebDriver driver;
	CommonFunction objCommonFunction;
	
	public String ordNwSIMCard ="//span[contains(text(),'Order a new SIM card.')]";
	//String ordNwSIMCard="//input[@value='Order a new SIM card.']";
	public String sm_continue ="//button[contains(@data-qa,'byod-simSelection-simCard-navigation-continueButton')]";
	
	public SimSelection(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver=driver;
		objCommonFunction = new CommonFunction(driver); 		
	}

	public boolean clickOnOrdNwSIMCard() {
		WebElement OrdNwSIM= objCommonFunction.getWebElement(ordNwSIMCard);
		if(OrdNwSIM== null)
			return false;
		else {			
			try {
				/*JavascriptExecutor executor = (JavascriptExecutor)driver;
				executor.executeScript("arguments[0].click();", OrdNwSIM);*/
				OrdNwSIM.click();
				//new Actions(driver).moveToElement(OrdNwSIM).click().build().perform();
				System.out.println("clickOnOrdNwSIMCard");			 	
			}
			catch(Exception e) {
				System.out.println(e);
				return false;				
			}			
		return true;			
		}			
	}
	
	public boolean clickOnSmContinue() {
		WebElement continueCTA = objCommonFunction.getWebElement(sm_continue);		
		if(continueCTA== null)
			return false;
		else {
			continueCTA.click();
			//new Actions(this.driver).moveToElement(continueCTA).click().build().perform();
			/*JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", continueCTA)*/;
			return true;			
		}		
		
	}

}
