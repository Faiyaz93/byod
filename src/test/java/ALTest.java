import org.testng.annotations.Test;


import byod.CommonFunction;
import byod.DeviceSelectionpage;
import byod.LoginPage;
import byod.Model;
import byod.NumberSelectionPage;
import byod.SimSelection;

import org.testng.annotations.BeforeTest;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class ALTest {
	WebDriver driver;
	CommonFunction objCommonFunction;
  @Test
  public void alByod() {
	  System.out.println("AAL");
	  Assert.assertTrue(objCommonFunction.openPage("https://att.com/buy/byod","Bring Your Own Phone or Device (BYOD) to AT&T"));
      
	  DeviceSelectionpage objDeviceSelectionpage = new DeviceSelectionpage(driver);
	  Assert.assertTrue(objDeviceSelectionpage.clickOnGetStarted());
	  Assert.assertTrue(objDeviceSelectionpage.clickOnPhones());
	  Assert.assertTrue(objDeviceSelectionpage.clickOnContinue());
	  
	  SimSelection objSimSelection = new SimSelection(driver);
	  System.out.println("Sim-Selection page");
	  Assert.assertTrue(objSimSelection.clickOnOrdNwSIMCard());
	  Assert.assertTrue(objSimSelection.clickOnSmContinue());
	  objCommonFunction.sleep();
	  	  
	  Model objModel = new Model(driver);
	  objCommonFunction.sleep();
	  objModel.clickOnSignIn();
	  LoginPage objLoginPage=new LoginPage(driver);
	  objLoginPage.login();
	  
	  NumberSelectionPage objNumberSelectionPage= new NumberSelectionPage(driver);
	  objNumberSelectionPage.selectNewNo();
	  objCommonFunction.sleep();
	  
  }
  @BeforeTest
  public void beforeTest() {
	 System.setProperty("webdriver.chrome.driver","C:\\Users\\faiyazs\\Desktop\\Selenium_Everything\\chrome-new\\chromedriver.exe");
	 
	 driver = new ChromeDriver();
	 driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;
	 objCommonFunction = new CommonFunction(driver);
	  /*ChromeOptions options = new ChromeOption	s();e
	  options.addArguments("--incognito");
	  DesiredCapabilities capabilities = DesiredCapabilities.chrome();
	  capabilities.setCapability(ChromeOptions.CAPABILITY, options);*/
	 
	/* Map<String, Object> deviceMetrics = new HashMap<>();

	 deviceMetrics.put("width", 360);

	 deviceMetrics.put("height", 640);

	 deviceMetrics.put("pixelRatio", 3.0);



	 Map<String, Object> mobileEmulation = new HashMap<>();

	 mobileEmulation.put("deviceMetrics", deviceMetrics);

	 mobileEmulation.put("userAgent", "Mozilla/5.0 (Linux; Android 4.2.1; en-us; Nexus 5 Build/JOP40D) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.166 Mobile Safari/535.19");

	 ChromeOptions chromeOptions = new ChromeOptions();
	 chromeOptions.setExperimentalOption("mobileEmulation", mobileEmulation);*/
	 
	  /*System.setProperty("webdriver.gecko.driver","C:\\Users\\faiyazs\\Desktop\\Selenium_Everything\\gecko\\geckodriver.exe");
	  driver = new FirefoxDriver();*/
}

  @AfterTest
  public void afterTest() {
	  //driver.close();	  
  
 }

}
