import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;

public class GoogleTest {
	WebDriver driver;
  @Test(priority=1,groups="test1")
  public void googleTitleTest() {
	  driver.get("https://www.google.com/");
	  Assert.assertEquals(driver.getTitle(), "Google", "title is not matched");
	  //int i =9/0;
  }
  @Test(priority=2,groups="test1" , dependsOnMethods="googleTitleTest")
  public void googleLinkTest() {
	  System.out.println("Link Test");	  
  }
  @Test(priority=3,groups="test1",dependsOnMethods="googleLinkTest")
  public void googleLogoTest() {
	  System.out.println("Logo Test");
	  }
  @Test(priority=4,groups="test2",invocationCount=3)
  public void googleSearchTest() {
	  System.out.println("Search Test");	  
  }
  
  @Test(priority=0,groups="test2" ,expectedExceptions=NumberFormatException.class)
  public void googleImageTest() {
	  System.out.println("Image Test");	  
  }
  @Test(groups="test3")
  public void googleSearchButtonTest() {
	  System.out.println("Button Test");	  
  }
  @Test(groups="test3")
  public void googleGmailTest() {
	  System.out.println("Gmail Test");	  
  }
  /**
 * 
 */
@BeforeMethod
  public void setUp() {
	  System.setProperty("webdriver.chrome.driver", "C:\\Users\\faiyazs\\Desktop\\Selenium_Everything\\work-space\\byod\\chrome\\chromedriver.exe");
	  driver=new ChromeDriver();
	  driver.manage().window().maximize();
	  driver.manage().deleteAllCookies();
	  driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
	  driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);	  
  }

  @AfterMethod
  public void tearDown() {
	  driver.quit();
  }

}
