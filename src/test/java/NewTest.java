import org.testng.annotations.Test;

import byod.CommonFunction;
import byod.CustomerInformation;
import byod.DeviceSelectionpage;
import byod.Model;
import byod.MyCart;
import byod.PageToLoad;
import byod.PlanPage;
import byod.SimSelection;

import org.testng.annotations.BeforeSuite;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;

public class NewTest {
	WebDriver driver;
	CommonFunction objCommonFunction;
	
  @Test
  public void newByod() {
	  
	  System.out.println("New cus");
	  //driver.get("http://tst02.stage.att.com/buy/phones");
	  //objCommonFunction.sleep();
	  Assert.assertTrue(objCommonFunction.openPage("https://tst12.stage.att.com/buy/byod/","Bring Your Own Phone or Device (BYOD) to AT&T"));
	  
	  DeviceSelectionpage objDeviceSelectionpage = new DeviceSelectionpage(driver);
	  Assert.assertTrue(objDeviceSelectionpage.clickOnGetStarted());
	  objCommonFunction.waitforElement(objDeviceSelectionpage.phones, driver);
	  Assert.assertTrue(objDeviceSelectionpage.clickOnPhones());
	  objCommonFunction.waitforElement(objDeviceSelectionpage.ds_continue, driver);
	  Assert.assertTrue(objDeviceSelectionpage.clickOnContinue());
	  
	  SimSelection objSimSelection = new SimSelection(driver);
	  System.out.println("Sim-Selection page");
	  Assert.assertTrue(objSimSelection.clickOnOrdNwSIMCard());
	  objCommonFunction.waitforElement(objSimSelection.sm_continue, driver);
	  Assert.assertTrue(objSimSelection.clickOnSmContinue());
	  //objCommonFunction.sleep();
	  PageToLoad objPageToLoad= new PageToLoad(driver);
	  objPageToLoad.waitForPageLoaded();
	  
	  	  
	  Model objModel = new Model(driver);
	  Assert.assertTrue(objModel.clickOnMdcontinue());
	  //objPageToLoad.waitForPageLoaded();
	  objCommonFunction.sleep();
	  Assert.assertTrue(objModel.enterZipCode());
	  objCommonFunction.sleep();
	  objPageToLoad.waitForPageLoaded();
	 
	  PlanPage objPlanPage =new PlanPage(driver);
	  Assert.assertTrue(objPlanPage.clickOnPlanCTA());
	  //objPageToLoad.waitForPageLoaded();
	 
	  objCommonFunction.sleep();
	 	  
      MyCart objMycart = new MyCart(driver);
      Assert.assertTrue(objMycart.clickOnCheckout());  
      
      objPageToLoad.waitForPageLoaded();
      
      CustomerInformation objCustomerInformation = new CustomerInformation(driver);
      objCommonFunction.waitforElement(objCustomerInformation.contactInfo_nextXpath, driver);
      Assert.assertTrue(objCustomerInformation.contactInfo());
    
      
      //objCommonFunction.sleep();
      objCommonFunction.waitforElement(objCustomerInformation.credit_NextXpath, driver);
      Assert.assertTrue(objCustomerInformation.creditCheck());
      
      objCommonFunction.waitforElement(objCustomerInformation.addressButtonXpath, driver);
      Assert.assertTrue(objCustomerInformation.shippingAddress());
      
      //objCommonFunction.waitforElement(objCustomerInformation.checkoutContinueXpath, driver);
      objCommonFunction.sleep();
      Assert.assertTrue(objCustomerInformation.checkoutSubmit());
      objCommonFunction.sleep();
      objPageToLoad.waitForPageLoaded();
      Assert.assertTrue(objCustomerInformation.selectANumber());
      objCommonFunction.sleep();
      Assert.assertTrue(objCustomerInformation.submitPaymentDetails());
      objCommonFunction.sleep();
      Assert.assertTrue(objCustomerInformation.submitCheckout());      
  }
   
  @BeforeSuite
  public void beforeSuite() {
//	  objCommonFunction = new CommonFunction(driver);
//	  objCommonFunction.launchDriver();
	  System.setProperty("webdriver.chrome.driver", "C:\\Users\\faiyazs\\Desktop\\Selenium_Everything\\work-space\\byod\\chrome\\chromedriver.exe");
	  /*ChromeOptions options = new ChromeOptions();
	  options.addArguments("--incognito");
	  DesiredCapabilities capabilities = DesiredCapabilities.chrome();
	  capabilities.setCapability(ChromeOptions.CAPABILITY, options);*/
	  driver= new ChromeDriver();
	  objCommonFunction = new CommonFunction(driver);
	  driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;	  
  }

  @AfterSuite
  public void afterSuite() {
	  //driver.close();
  
  }
}
