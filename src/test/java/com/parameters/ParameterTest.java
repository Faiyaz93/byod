package com.parameters;

import org.testng.annotations.Test;




import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;

public class ParameterTest {
	WebDriver driver;
  @Test
  @Parameters({"id","password"})
  public void gmailLogin(@Optional("abc") String id,@Optional("cde") String password) {
	  driver.findElement(By.xpath("//*[@id=\"email\"]")).clear();
	  driver.findElement(By.xpath("//*[@id=\"email\"]")).sendKeys(id);
	  driver.findElement(By.xpath("//*[@id=\"email\"]")).clear();
	  driver.findElement(By.xpath("//*[@id=\"pass\"]")).sendKeys(password);
	  driver.findElement(By.xpath("//*[@id=\"u_0_8\"]")).click();	  
  }
  @BeforeMethod
  public void beforeLogin() {
	  System.setProperty("webdriver.chrome.driver", "C:\\Users\\faiyazs\\Desktop\\Selenium_Everything\\work-space\\byod\\chrome\\chromedriver.exe");
	  driver=new ChromeDriver();
	  
	  driver.get("https://www.facebook.com/");
  }

  @AfterMethod
  public void afterafter() {
	  driver.quit();
  }

}
